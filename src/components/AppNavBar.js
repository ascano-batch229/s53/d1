import NavBar from 'react-bootstrap/NavBar';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom';


export default function	AppNavBar(){
	return (
		<NavBar bg="light" expand="lg" className="p-3">
			<NavBar.Brand as={NavLink} exact to="/"> Zuitt </NavBar.Brand>
			<NavBar.Toggle aria-controls="basic-navbar-nav"/> 
			<NavBar.Collapse id="basic-navbar-nav">	
				<Nav className="ml-auto">
					<Nav.Link as={NavLink} exact to="/"> Home </Nav.Link>
					<Nav.Link as={NavLink} exact to="/courses"> Courses</Nav.Link>
					<Nav.Link as={NavLink} exact to="/login"> Login </Nav.Link>
					<Nav.Link as={NavLink} exact to="/register"> Register </Nav.Link>
					

				</Nav>

			</NavBar.Collapse>
		</NavBar>


		)
}