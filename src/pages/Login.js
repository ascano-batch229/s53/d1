import { Form, Button } from 'react-bootstrap';
import {useState, useEffect} from 'react';

export default function Login(){

	// State Hooks - Store values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false)

	console.log(email);
	console.log(password1);
	console.log(password2);

	useEffect(() => {
		// Validation to enable register button when all fields are populated and both password match
		if(email !== '' && password1 !== ''){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	})

	// Function to simulate user registration

	function registerUser(e){

		// Prevents page redirection via form submission
		e.preventDefault();

		setEmail('');
		setPassword1('');
		setPassword2('');

		alert("You are now logged in!");

	}

	return(

		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group className="mb-3" controlId="userEmail">

				<h1>Login</h1>
				<Form.Label>Email address</Form.Label>
				<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>

			</Form.Group>

			<Form.Group className="mb-3" controlId="password1" >
				<Form.Label>Password</Form.Label>
				<Form.Control type="password" placeholder="Password"  value={password1} onChange={e => setPassword1(e.target.value)} required/>
			</Form.Group>


				{/*Conditional Rendering -> IF ACTIVE BUTTON IS CLICKABLE -> IF INACTIVE BUTTON IS NOT CLICKABLE*/}
				{
					(isActive)?
					<Button variant="primary" type="submit" controlId="submitBtn">
						Login
					</Button>
					:
					<Button variant="primary" type="submit" controlId="submitBtn" disabled>
						Login
					</Button>										

				}
		</Form>


		) 


}